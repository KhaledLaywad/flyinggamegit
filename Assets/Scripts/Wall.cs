﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

	public Transform Player;
	public GameObject wall;

	void Start () 
	{
		if((transform.position.x - Player.position.x) < 60)
		Instantiate(Resources.Load("Wall"),new Vector3(transform.position.x + 1,transform.position.y,transform.position.z),Quaternion.identity);
		else 
		StartCoroutine("Wait");

	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds(.05f);
		Instantiate(Resources.Load("Wall"),new Vector3(transform.position.x + 1,transform.position.y,transform.position.z),Quaternion.identity);

	}



}
