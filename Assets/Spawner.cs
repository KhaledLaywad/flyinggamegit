﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public float waitTime = 0.5f;
	void Start () {
	StartCoroutine("Spawn");
	}
	
	IEnumerator Spawn()
	{
		Instantiate(Resources.Load("Obstacle"),new Vector3(transform.position.x,Random.Range(-4.5f,4.5f)),Quaternion.identity);
		yield return new WaitForSeconds(waitTime);
		StartCoroutine("Spawn");

	}
}
